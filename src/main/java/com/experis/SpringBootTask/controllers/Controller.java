package com.experis.SpringBootTask.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    /**
     * This method is run when the endpoint with "/hello" is accessed with the GET method.
     * It returns a string to greet the user using the input from the endpoint,
     * or tells the user that no input was given if the given input is empty
     * @param input the user's input from the endpoint
     * @return a greeting or info that no input was given
     */
    @RequestMapping(value="/hello")
    public String greetUser(@RequestParam String input) {
        if(input.isEmpty()) {
            return "No input given";
        }
        return "Greetings, " + input + "!!";
    }

    /**
     * This method is run when the endpoint with "/reverse" is accessed with the GET method.
     * It reverses the input string given from the endpoint,
     * or tells the user that no input was given if the given input is empty
     * @param input the user's input from the endpoint
     * @return the same text as given input but in reverse, or info that no input was given
     */
    @RequestMapping(value="/reverse")
    public String reverseInput(@RequestParam String input) {
        if(input.isEmpty()) {
            return "No input given";
        }
        StringBuilder builder = new StringBuilder(input);
        return builder.reverse().toString();
    }

    /**
     * This method is run when the endpoint with "/encode" is accessed with the GET method.
     * It encodes the input from the endpoint, using the famous language "Rövarspråket".
     * Whenever there's a consonant, an 'o' and the consonant is added.
     * Returns a string telling the user that the input was empty if that was the case
     *
     * For example,
     * Input: jessica
     * Output: jojesossosicoca
     * @param input the user's input from the endpoint
     * @return a string containing the input in Rövarspråket, or info that no input was given
     */
    @RequestMapping(value="/encode")
    public String encodeInput(@RequestParam String input) {
        if(input.isEmpty()) {
            return "No input given";
        }
        String inputTrimmed = input.trim();
        String newString = "";
        char ch;

        for(int i=0; i<inputTrimmed.length(); i++) {
            ch = inputTrimmed.charAt(i);
            if (ch==' ') {} else if (ch == 'a' || ch == 'o' || ch == 'u' || ch == 'å'
                    || ch == 'e' || ch == 'i' || ch == 'y' || ch == 'ä' || ch == 'ö') {
                newString+= ch;
            } else {
                newString+= ch + "o" + ch;
            }
        }
        return newString;
    }
}
